use std::fs::{self, File};
use std::io::{Write, Error as IOError};

pub fn read_file(file_path: &str) -> Result<String, IOError>{
    let file_reader = fs::read_to_string(file_path)?;
    Ok(file_reader)
}

#[test]
fn read_file_test(){
    assert_eq!(read_file("example.txt").unwrap(), "TEST\n");
}

pub fn clear_bridges(torrc_text: &String) -> String {
    let splited_torrc_text = torrc_text.split("\n");
    let mut new_torrc_text = String::new();

    for i in splited_torrc_text{
        if !i.contains("Bridge ") {
            new_torrc_text += format!("\n{}", i).as_str();
        }
    }
    
    new_torrc_text
}

#[test]
fn clear_bridges_test(){
    let a1 = clear_bridges(&"Bridge ACAB\nTEST\n".to_string());
    assert_eq!(a1, "\nTEST\n");
}

pub fn get_bridges(torrc_text: &String) -> String {
    let splited_torrc_text = torrc_text.split("\n");
    let mut new_torrc_text = String::new();

    for i in splited_torrc_text{
        if i.contains("Bridge ") {
            new_torrc_text += format!("\n{}", i).as_str();
        }
    }
    
    new_torrc_text
}

#[test]
fn get_bridges_test(){
    let a1 = get_bridges(&"Bridge ACAB\nTEST\n".to_string());
    assert_eq!(a1, "\nBridge ACAB");
}

pub fn add_new_bridges(torrc_text: &String, bridges_text: &String) -> String {
    let bridges_text_new = bridges_text.replace("Bridge ", "");
    let mut append_text = String::new();
    for i in bridges_text_new.split("\n"){
        if i.contains("obfs4 ") {
            append_text += format!("Bridge {}\n", i).as_str();
        }
    }
    format!("{}{}", torrc_text, append_text)
}

#[test]
fn add_new_bridges_test(){
    assert_eq!(add_new_bridges(&"TEST\n".to_string(), &"obfs4 ACAB".to_string()), "TEST\nBridge obfs4 ACAB\n");
    assert_eq!(add_new_bridges(&"TEST\n".to_string(), &"obfs4 ACAB\nACAX".to_string()), "TEST\nBridge obfs4 ACAB\n");
}

pub fn write_file(write_text: String, path: &str) -> Result<(), IOError> {
    let mut output = File::create(path)?;
    output.write(write_text.as_bytes())?;
    Ok(())
}

#[test]
fn write_file_test() {
    write_file("TEST2\n".to_string(), "write_test2.txt").unwrap()
}

#[test]
fn open_file_clear_bridge_insert_new_test(){
    let mut text_torrc = read_file("/etc/tor/torrc").unwrap();
    let text_bridge = read_file("bridges").unwrap();
    text_torrc = clear_bridges(&text_torrc);
    let final_text = add_new_bridges(&text_torrc, &text_bridge);
    write_file(final_text, "test3.txt").unwrap();
}
